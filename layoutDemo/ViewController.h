//
//  ViewController.h
//  layoutDemo
//
//  Created by Ankur on 06/02/15.
//  Copyright (c) 2015 your company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController{

    __weak IBOutlet UIButton *threehourButton;
    __weak IBOutlet UIButton *sixhourButton;
    __weak IBOutlet UIButton *twelvehourButton;
    __weak IBOutlet UIButton *twenty4hourButton;

}


@end

