//
//  ViewController.m
//  layoutDemo
//
//  Created by Ankur on 06/02/15.
//  Copyright (c) 2015 your company. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSLayoutConstraint *equalWidth = [NSLayoutConstraint
                  constraintWithItem:threehourButton
                  attribute:NSLayoutAttributeWidth
                  relatedBy:NSLayoutRelationEqual
                  toItem: nil
                  attribute:NSLayoutAttributeWidth
                  multiplier:1.0f
                  constant:threehourButton.frame.size.width];
    
    NSLayoutConstraint *equalHeight = [NSLayoutConstraint
                                      constraintWithItem:threehourButton
                                      attribute:NSLayoutAttributeWidth
                                      relatedBy:NSLayoutRelationEqual
                                      toItem: nil
                                      attribute:NSLayoutAttributeWidth
                                      multiplier:1.0f
                                      constant:threehourButton.frame.size.height];
    
    NSLayoutConstraint *leading = [NSLayoutConstraint
                                       constraintWithItem:threehourButton
                                       attribute:NSLayoutAttributeWidth
                                       relatedBy:NSLayoutRelationEqual
                                       toItem: nil
                                       attribute:NSLayoutAttributeWidth
                                       multiplier:1.0f
                                       constant:threehourButton.frame.size.height];
    
    NSLayoutConstraint *trailing = [NSLayoutConstraint
                                   constraintWithItem:threehourButton
                                   attribute:NSLayoutAttributeWidth
                                   relatedBy:NSLayoutRelationEqual
                                   toItem: nil
                                   attribute:NSLayoutAttributeWidth
                                   multiplier:1.0f
                                   constant:threehourButton.frame.size.height];
    
    NSLayoutConstraint *top = [NSLayoutConstraint
                                   constraintWithItem:threehourButton
                                   attribute:NSLayoutAttributeWidth
                                   relatedBy:NSLayoutRelationEqual
                                   toItem: nil
                                   attribute:NSLayoutAttributeWidth
                                   multiplier:1.0f
                                   constant:threehourButton.frame.size.height];
    
    
   // [headingButton setTitleColor:[UIColor colorWithRed:36/255.0 green:71/255.0 blue:113/255.0 alpha:1.0] forState:UIControlStateNormal];
    
   /*
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:threehourButton
                                                                  attribute:NSLayoutAttributeLeading
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:threehourButton.superview
                                                                  attribute:NSLayoutAttributeLeading
                                                                 multiplier:1
                                                                   constant:-12];
    [threehourButton addConstraint:constraint];
  */
    //feature 3 solved
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
