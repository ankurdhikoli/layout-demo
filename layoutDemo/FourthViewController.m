//
//  FourthViewController.m
//  layoutDemo
//
//  Created by Ankur on 17/02/15.
//  Copyright (c) 2015 your company. All rights reserved.
//

#import "FourthViewController.h"
#import "dynamicView.h"

@interface FourthViewController ()

@end

@implementation FourthViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
 //   dynamicView * dynamicObj=[[dynamicView alloc]init];
 //   [self.view addSubview:dynamicObj];
    
    
    NSArray * allTheViewsInMyNIB = [[NSBundle mainBundle] loadNibNamed:@"dynamicView" owner:self options:nil]; // loading nib (might contain more than one view)
    dynamicView* problemView = allTheViewsInMyNIB[0]; // getting desired view
    
    
    /*
     [self.view addConstraint:[NSLayoutConstraint constraintWithItem:problemView
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.view
                                                              attribute:NSLayoutAttributeBottom
                                                             multiplier:1.0
                                                               constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:problemView
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1.0
                                                           constant:0.0]];
*/
    problemView.frame = CGRectMake(0,64,self.view.bounds.size.width,100); //setting the frame
    [self.view addSubview:problemView]; // showing it to user

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
